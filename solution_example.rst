
.. code:: python

    cd ..


.. parsed-literal::

    /home/pablo/Programming/papers


.. code:: python

    from dolo import yaml_import, pcat, display

.. code:: python

    
    model = yaml_import("models/integration_B.yaml")
    model


.. parsed-literal::

    Model type detected as 'dtmscc'




.. parsed-literal::

    
    Model object:
    ------------
    
    - name: "Partial Integration"
    - type: "dtmscc"
    - file: "models/integration_B.yaml
    
    - residuals:
    
        transition
            1   : 0.0000 : k_1 =        ( (1-delta) + Phi_1(-1) )*k_1(-1) / exp(E)
            2   : 0.0000 : k_2 =        ( (1-delta) + Phi_2(-1) )*k_2(-1) / exp(E)
            3   : 0.0000 : b_f = db_f(-1) / exp(E)
    
        arbitrage
            1   : 0.0000 : rho_1 - rho_2  |   min_bb <= db_f <= max_bb
            2   : 0.0000 : p_f = lam*rho_1 + (1-lam)*rho_2
            3   : 0.0000 : rho_1 * r_I_1 - 1                     |   0.00 <= i_1 <= inf
            4   : 0.0000 : rho_2 * r_I_2 - 1                     |   0.00 <= i_2 <= inf
            5   : 0.0000 : 1 = (  w_1(1)/ew_1*exp(E(1)) )**(1-gamma)
            6   : 0.0000 : 1 = (  w_2(1)/ew_2*exp(E(1)) )**(1-gamma)
            7   : 0.0000 : beta - (w_1/ew_1)**(1-psi) + (1-beta)*(c_1/ew_1)**(1-psi)
            8   : 0.0000 : beta - (w_2/ew_2)**(1-psi) + (1-beta)*(c_2/ew_2)**(1-psi)
    
        auxiliary
            1   : 0.0000 : y_1 = k_1**theta*exp(A_1)
            2   : 0.0000 : y_2 = k_2**theta*exp(A_2)
            3   : 0.0000 : c_1 = y_1 - i_1 +  b_f - p_f*db_f
            4   : 0.0000 : c_2 = y_2 - i_2 - (b_f - p_f*db_f)/S_2*S_1
            5   : 0.0000 : Phi_1 = a1 + a2/(1-xi)*(i_1/k_1)**(1-xi)
            6   : 0.0000 : Phi_2 = a1 + a2/(1-xi)*(i_2/k_2)**(1-xi)
            7   : 0.0000 : Phi_1__p =  a2*(i_1/k_1)**(-xi)
            8   : 0.0000 : Phi_2__p =  a2*(i_2/k_2)**(-xi)
    
        value
            1   : 0.0000 : roc_1 = r_I_1
            2   : 0.0000 : roc_2 = r_I_2
            3   : 0.0000 : p_S_1 = rho_1 * (p_S_1(1) + theta*y_1(1)-i_1(1) )*exp(E(1))
            4   : 0.0000 : p_S_2 = rho_2 * (p_S_2(1) + theta*y_2(1)-i_2(1) )*exp(E(1))
            5   : 0.0000 : r_S_1 = exp(E(1))*(p_S_1(1) + theta*y_1(1)-i_1(1) )/p_S_1
            6   : 0.0000 : r_S_2 = exp(E(1))*(p_S_2(1) + theta*y_2(1)-i_2(1) )/p_S_2




.. code:: python

    model.set_calibration(N_k=10, N_b=10)
    model.set_calibration(n_a=2)             # set the number of values for the productivity process

discretization of the markov variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: python

    P, Q = model.markov_chain


Each line of ``P`` represents one value for the vector ``(A_1, A_2, E)``

.. code:: python

    display(P)
    display('Shape: {}'.format(P.shape))



.. parsed-literal::

    array([[-0.05735393, -0.11470787,  0.        ],
           [-0.05735393,  0.11470787,  0.        ],
           [ 0.05735393, -0.11470787,  0.        ],
           [ 0.05735393,  0.11470787,  0.        ]])



.. parsed-literal::

    'Shape: (4, 3)'


We can see that there are ``N_m=4`` different values for the markov
process. The transition matrix ``Q`` is such that for any ``i,j``, the
cell ``Q[i,j]`` is the probability of jumping from state ``i`` to state
``j``.

.. code:: python

    display(Q)
    display('Sum of each row: {}'.format(Q.sum(axis=1)))



.. parsed-literal::

    array([[ 0.9025,  0.0475,  0.0475,  0.0025],
           [ 0.0475,  0.9025,  0.0025,  0.0475],
           [ 0.0475,  0.0025,  0.9025,  0.0475],
           [ 0.0025,  0.0475,  0.0475,  0.9025]])



.. parsed-literal::

    'Sum of each row: [ 1.  1.  1.  1.]'


state-space for the continuous variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The data defining the continuous state-space is contained in the
``model.options['approximation_space']`` field

.. code:: python

    ap = model.options['approximation_space']
    display(ap)



.. parsed-literal::

    {'a': [2, 2, -10.0], 'b': [10, 10, 10.0], 'orders': [10, 10, 10]}


It contains the lower- and upper-bounds (keys ``a`` and ``b``) for the
vector of continuous states ``(k_1, k_2, b_f)``. The number of nodes
along each dimension is defined by key ``orders``. The cartesian grid is
represented as a ``Nxn_s`` grid, where ``N=10*10*10`` is the number of
points of the cartesian product and ``n_x=3`` the number of states. Each
line corresponds to a different point in the state-space. The cartesian
product operates on the last index first.

.. code:: python

    from dolo.numeric.misc import mlinspace
    grid = mlinspace(ap['a'], ap['b'], ap['orders'])
    print('Size of the grid {}'.format(grid.shape))
    print('Columns: (k_1, k_2, b_f)')
    print("First 15 elements: ")
    print(grid[:15,:])



.. parsed-literal::

    Size of the grid (1000, 3)
    Columns: (k_1, k_2, b_f)
    First 15 elements: 
    [[  2.           2.         -10.        ]
     [  2.           2.          -7.77777778]
     [  2.           2.          -5.55555556]
     [  2.           2.          -3.33333333]
     [  2.           2.          -1.11111111]
     [  2.           2.           1.11111111]
     [  2.           2.           3.33333333]
     [  2.           2.           5.55555556]
     [  2.           2.           7.77777778]
     [  2.           2.          10.        ]
     [  2.           2.88888889 -10.        ]
     [  2.           2.88888889  -7.77777778]
     [  2.           2.88888889  -5.55555556]
     [  2.           2.88888889  -3.33333333]
     [  2.           2.88888889  -1.11111111]]


Defining the first order guess
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The solution to the model must be a function ``f(i,s)``. These arguments
are: - ``i``: the index of a markov state - ``s``: a list of ``M``
different states vectors by an ``Mx3`` matrix. The function must return
a list of ``M`` controls vectors as an ``Mxn_x`` vector (there are
``n_x=8`` controls)

In order to start the iteration process, some initial guess, for this
solution must be provided. The initial guess function is provided in the
file ``crw_code/make_initial_guess.py``.

.. code:: python

    from crw_code.make_initial_guess import initial_dr_int
    # other options are initial_dr_aut and initial_dr_comp for autarky and completemarkets respectively
    dri = lambda i,s: initial_dr_int(model, i, s) # constructs the function
    vals = dri(0, grid[:15,:])
    print("Values of the controls on the 15 first points of the grid: (shape {})".format(vals.shape))
    print(vals)


.. parsed-literal::

    Values of the controls on the 15 first points of the grid: (shape (15, 8))
    [[-10.           0.96         0.16         0.16         0.67114441
        1.47114441   0.67114441   1.47114441]
     [ -7.77777778   0.96         0.16         0.16         0.7600333
        1.38225552   0.7600333    1.38225552]
     [ -5.55555556   0.96         0.16         0.16         0.84892219
        1.29336664   0.84892219   1.29336664]
     [ -3.33333333   0.96         0.16         0.16         0.93781108
        1.20447775   0.93781108   1.20447775]
     [ -1.11111111   0.96         0.16         0.16         1.02669997
        1.11558886   1.02669997   1.11558886]
     [  1.11111111   0.96         0.16         0.16         1.11558886
        1.02669997   1.11558886   1.02669997]
     [  3.33333333   0.96         0.16         0.16         1.20447775
        0.93781108   1.20447775   0.93781108]
     [  5.55555556   0.96         0.16         0.16         1.29336664
        0.84892219   1.29336664   0.84892219]
     [  7.77777778   0.96         0.16         0.16         1.38225552
        0.7600333    1.38225552   0.7600333 ]
     [ 10.           0.96         0.16         0.16         1.47114441
        0.67114441   1.47114441   0.67114441]
     [-10.           0.96         0.16         0.23111111   0.67114441
        1.54362472   0.67114441   1.54362472]
     [ -7.77777778   0.96         0.16         0.23111111   0.7600333
        1.45473583   0.7600333    1.45473583]
     [ -5.55555556   0.96         0.16         0.23111111   0.84892219
        1.36584694   0.84892219   1.36584694]
     [ -3.33333333   0.96         0.16         0.23111111   0.93781108
        1.27695805   0.93781108   1.27695805]
     [ -1.11111111   0.96         0.16         0.23111111   1.02669997
        1.18806916   1.02669997   1.18806916]]


Solving the model
^^^^^^^^^^^^^^^^^

The actual solution code is written in the time\_iteration fuction. When
called with the ``verbose=True`` switch it produces the following output
reads as follows: - first column is the iteration number ``n`` at which
the matrix of controls :math:`X_n` is solved - second columns is the
distance :math:`X_n - X_{n-1}` - third column is the ratio
:math:`\frac{X_n - X_{n-1}}{X_{n-1} - X_{n-2}}` - fourth column is the
duration of each iteration - last column is the number of newton steps
taken to optimize the residuals (more informations with
``verbose='full'``) To limit the size of the output, we stop after 5
iterations.

.. code:: python

    from crw_code.time_iteration import time_iteration
    dr = time_iteration(model, initial_guess=dri, verbose=True, maxit=5)


.. parsed-literal::

    Solving WITH complementarities.
    ------------------------------------------------
    | N   |  Error     | Gain     | Time     | nit |
    ------------------------------------------------
    |   1 |  6.604e-01 |      nan |    6.597 |  10 |
    |   2 |  2.077e-01 |    0.314 |    4.435 |   8 |
    |   3 |  1.527e-01 |    0.735 |    4.148 |   8 |
    |   4 |  1.154e-01 |    0.756 |    4.124 |   9 |
    |   5 |  8.080e-02 |    0.700 |    3.698 |   8 |
    ------------------------------------------------
    Elapsed: 23.0045042038 seconds.
    ------------------------------------------------


The object returned by the ``time_iteration`` function is a decision
rule object, which can be used to compute controls at any state:

.. code:: python

    vals = dr(0, grid[:15,:])
    print('shape : {}'.format(vals.shape))


.. parsed-literal::

    shape : (15, 8)


In particular, it can be used as an initial guess for a different
calibration or a modfied state-space. Here we finish the iterations that
were previously stopped after 5 rounds:

.. code:: python

    dr = time_iteration(model, initial_guess=dr, verbose=False)


.. parsed-literal::

    Solving WITH complementarities.


Simulations
^^^^^^^^^^^


Accuracy checks
^^^^^^^^^^^^^^^

