Numerical appendix for "Financial Integration in a Risky World"
===============================================================

.. contents:: Table of Contents
   :depth: 3

Installation and dependencies
-----------------------------


Dependencies
~~~~~~~~~~~~

An up-to-date version of the code is hosted at ``github/address``.

The following software is required in order to replicate the results:

- a scientific Python distribution, for instance `Anaconda <https://store.continuum.io/cshop/anaconda/>`__
- `dolo <http://econforge.github.com/dolo/>`__: to import model files only. The actual solution code is included with this project
- (optional) `tmux <https://tmux.github.io/>`__: to monitor the parallel solutions of many calibrations
- (optional) `runipy <https://github.com/paulgb/runipy>`__: to run all notebooks automatically
- ipyparallel

Project structure
~~~~~~~~~~~~~~~~~

By convention all paths given below are relative to the root of the project, that is the directory which contains
the ``README.md`` file. Further documentation, including this curent document can be found in the ``doc/`` directory and is generated from the source files in ``doc_source/``.

Inputs: models and calibrations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The models and calibrations solved in the paper, are coded using `dolo <http://econforge.github.com/dolo/>`__ conventions and conceptually separated from the more algorithmic code.

- models are stored in ``models/`` this includes:

  - ``models/integration_A.yaml``: two countries in autarky
  - ``models/integration_B.yaml``: incomplete integration
  - ``models/integration_C.yaml``: complete integration
  - ``models/endowments_A.yaml``: two countries in autarky
  - ``models/endowments_B.yaml``: incomplete integration
  - ``models/endowments_C.yaml``: complete integration

- list of calibration scenarios:

  - ``models/calibration_normal.yaml``: variations on the main parameters
  - ``models/calibration_long_run.yaml``: long run risk experiments



Outputs: solutions, simulations and post-processing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The results presented are computed in several steps. All model *solutions*, i.e. an approximation to the decision rule are computed by solving a time-iteration problem. Then using these results, the various model calibrations are *simulated* to produce impulse response functions, as well as random time series. These simulations are stored on disk to facilitate further examination and typical business cycle statistics are produced in Excel files. Finally, graphs produced for the paper using either decision rules or simulations are left in the ``tables`` directory.
Here is a detailed summary of output files:


- solutions (include decision rule and value functions):

  - ``results/solutions_A``: autarky calibrations
  - ``results/solutions_B``: incomplete integration
  - ``results/solutions_C``: complete integration

- simulations (deterministic and stochastic):

  - ``results/all_simulations``

- tables (Excel format):

- graphs:

Code and algorithms
^^^^^^^^^^^^^^^^^^^

All code used to solve the model, written in Python is included in the ``code`` folder. Most of it is generic and could easily be adapted. It is described with more details in section (code_).

- detail ?

In order to produce all results, the following scrips can be run in turn:

- Computing the model solutions: run either

  - ``sh run_all_solutions.sh``: solves all calibrations of the model (takes several days)
  - ``python download_all_solutions.py``: downloads precomputed solutions from the `internet <https://dl.dropboxusercontent.com/u/21505234/crw_data/>`_

- Performing the simulations: run either

  - ``sh run_all_simulations.sh``: compute impulse-response functions and stochastic simulations for all calibrations (takes a few hours)
  - ``python download_all_simulations.py``: downloads precomputed simulations from the `internet <https://dl.dropboxusercontent.com/u/21505234/crw_data/>`_


.. A script is provided to solve all calibrations of the model: ``run_all_solutions.sh``. On a real world machine these computations take several days to compute. To avoid this long delay, it is possible to download the solutions by running ``download_all_solutions.sh``. If ``run_all_solutions.sh`` is run afterwards, it will then use these solutions as initial guess, and check that they are an actual local solution.

..  After all solutions are computed, all simulations can be run with the command:  ``run_all_simulations.sh``. Again, this take a while to complete, and it is also possible to download the the data instead with ``download_all_simulations.sh``.

- To perform the post-processing steps run the scripts:

  - ``make_all_graphs.sh``: compute all figures from the paper
  - ``make_all_tables.sh`` compute all business statistics tables

The actual implementation of the post-processing steps is made in independent Jupyter notebooks contained in the ``notebooks`` directory. They are self-documented and can be run separately as well as browsed online (see link).



General algorithm
-----------------

Model
~~~~~

In this section we describe the incomplete market version of the model with capital. This version is defined by the file ``models/integration_B.yaml`` whose extract we comment here.

Following the classification used by dolo, the model falls in the category of Discrete-Time Mixed States (i.e. markovian and continuous states) with Continuous Controls.

The section ``symbols:`` of ``models/integration_B.yaml`` lists symbols appearing in the model by type:

.. code:: yaml
  :number-lines: 5

  symbols:
        markov_states: [A_1, A_2, E]
        states: [k_1, k_2, b_f]
        controls: [db_f, p_f, i_1, i_2, ew_1, ew_2, w_1, w_2]
        auxiliaries:  [y_1, y_2, c_1, c_2, Phi_1, Phi_2, Phi_1__p, Phi_2__p]
        values: [roc_1, roc_2, p_S_1, p_S_2, r_S_1, r_S_2]
        parameters: [beta, theta, gamma, psi, delta, rho_A, rho_E, min_b, max_b,
            a1, a2, xi, sigma_A_1, sigma_A_2, sigma_E, kmin, kmax, min_bb, max_bb, spmin, spmax,
            S_1, S_2, country, zeta, hom]

Parameters (``parameters:``) are constant values. Table X lists, all of them together with their calibration, which appears in the ``calibration:`` subsection of the model file. Note that the table also lists "Solution parameters". These are the parameters that impact the numerical  solution of the model but do not appear in the model equations per se.

==================  ============  ======================================================= =====================================================
Symbol group        Symbol          Calibration                                               Description
==================  ============  ======================================================= =====================================================
Parameters          beta            0.96                                                      Time discount
    .               theta           0.3                                                       Capital share
    .               gamma           4.0                                                       Risk aversion
    .               psi             4.0                                                       1/EIS
    .               delta           0.08                                                      Capital depreciation
    .               rho_A           0.9                                                       Productivity persistence
    .               rho_E           0.99                                                      Long run risk persistence
    .               min_b           -10.0                                                     Lower limit for debt (state-space)
    .               max_b            10.0                                                     Upper limit for debt (state-space)
    .               a1              ``delta - a2/(1-xi)*delta^(1-xi)``                        Capital adjustment cost (2)
    .               a2              ``delta^(xi)``                                            Capital adjustment cost (3)
    .               xi              0.2                                                       Capital adjustment cost
    .               sigma_A_1       0.025                                                     Productivity std. deviation (country 1)
    .               sigma_A_2       0.05                                                      Productivity std. deviation (country 2)
    .               sigma_E         0.00                                                      Long run risk volatility
    .               kmin            2.0                                                       Lower bound for capital
    .               kmax            10.0                                                      Upper bound for capital
    .               min_bb           -10.0                                                    Lower limit for capital (borrowing constraint)
    .               max_bb            10.0                                                    Upper limit for capital (borrowing constraint)
    .               S_1                1.0                                                    Size of country 1
    .               S_2                1.0                                                    Size of country 2
    .               country            0                                                      Which model to solve [Autarky case only]
    .               zeta             0.0                                                      Correlation of productivity processess
    .               hom              1.0                                                      Homotopy parameter
------------------  ------------  ------------------------------------------------------- -----------------------------------------------------
Solution options    N_k            30                                                        Number of grid points for capital
    .               N_b            30                                                        Number of grid points for debt
    .               n_a            3                                                         Number of values for discretized output
    .               n_e            1                                                         Number of values for discretized long run risk
==================  ============  ======================================================= =====================================================



The vector of exogenous states :math:`m_t` introduced by ``markov_states:`` consists in the productivity processes :math:`A_{1,t}` and :math:`A_{2,t}` as well as the permanent component of productivity :math:`E_t`. The vector of continuous states :math:`s_t` (simply ``states:`` in the model file) consists in the levels of capital :math:`k_{1,t}` and :math:`k_{2,t}` as well as debt inherited from last period :math:`b_t` (in the incomplete markets case).

The solution of the model is an endogenous vector :math:`x_t` or variables which are an unknown function :math:`\varphi(m_t,s_t)` of the states. They are declared as ``controls:``. In this case, the endogenous variables to be determined are primarly the amount of new lending, denoted by ``db_f`` in the model file, the price of debt :math:`p_{f,t}` the investment in both countries :math:`i_1` and :math:`i_2`. Since Euler equations with Epstein-Zin utility functions imply the value functions for both countries :math:`w_{1,t}` and :math:`w_{2,t}` we add them to the list of controls, as well as their quasi-expectations :math:`{E_t \left[ {w_1}^\alpha \right]}^{\frac{1}{\alpha}}` and :math:`E_t \left[ {w_2}^\alpha \right]^{\frac{1}{\alpha}}` denoted by ``ew_1`` and ``ew_2`` respectively.

By convenience, we also define a vector :math:`y_t` of auxiliary variables (``auxiliaries:``) which are simple functions of contemporaneous states and controls, and can essentially be replaced everywhere. Also, we treat separately the vector :math:`v_t` (``values:``)  of special variables that can be solved for as a direct iterative process (i.e. without optimization) after ``controls`` have been solved. Note that these are *not* the values in the optimization programs of the two agents (i.), which are solved together with the Euler equations. Instead they consist in various prices defined recursively.

All the steady-state values of endogenous variables are defined in the ``calibration:`` section of the model file and reported in table Y.

==================  ============  ======================================================= =====================================================
Symbol group        Symbol          Steady-state value                                         Description
==================  ============  ======================================================= =====================================================
Markov states       A_1              0.0                                                      Productivity in Country 1
.                   A_2              0.0                                                      Productivity in Country 2
.                   E                0.0                                                      Long run risk (steady state)
------------------  ------------  ------------------------------------------------------- -----------------------------------------------------
Continuous states   k_1              ``i_1/delta``                                            Capital (country 1)
.                   k_2              ``i_2/delta``                                            Capital (country 2)
.                   b_f                0.0                                                    Outstanding debt (country 1)
------------------  ------------  ------------------------------------------------------- -----------------------------------------------------
Controls            db_f            ``0``                                                     New debt (country 1)
    .               p_f            ``beta``                                                   Price of riskfree bond
    .               i_1            ``((1/beta-(1-delta))/theta)**(1/(theta-1)) * delta``      Investment (country 1)
    .               i_2            ``((1/beta-(1-delta))/theta)**(1/(theta-1)) * delta``      Investment (country 2)
    .               w_1            ``c_1``                                                    Welfare (country 1)
    .               w_2            ``c_2``                                                    Welfare (country 2)
    .               ew_1            ``c_1``                                                   Quasi-expected welfare (country 1)
    .               ew_2            ``c_2``                                                   Quasi-expected welfare (country 2)
------------------  ------------  ------------------------------------------------------- -----------------------------------------------------
Auxiliaries         y_1            ``k_1**theta``                                               Output (country 1)
  .                 y_2            ``k_2**theta``                                               Output (country 2)
  .                 c_1            ``(y_1 - delta*k_1)``                                        Consumption (country 1)
  .                 c_2            ``(y_2 - delta*k_2)``                                        Consumption (country 2)
  .                 Phi_1          ``a1 + a2/(1-xi)*(i_1/k_1)^(1-xi)``                          Adjustment cost (country 1)
  .                 Phi_2          ``a1 + a2/(1-xi)*(i_2/k_2)^(1-xi)``                          Adjustment cost (country 2)
  .                 Phi_1__p       ``a2*(i_1/k_1)^(-xi)``                                       Diff. adjustment cost (country 1)
  .                 Phi_2__p       ``a2*(i_2/k_2)^(-xi)``                                       Diff. adjustment cost (country 2)
------------------  ------------  ------------------------------------------------------- -----------------------------------------------------
Values               p_1             ``beta``                                               Risk-free bond priced by country 1
 .                   p_2             ``beta``                                               Risk-free bond priced by country 2
 .                   p_S_1             ``beta/(1-beta)*(theta*y_1-i_1)``                    Price of claim on net output (country 1)
 .                   p_S_2             ``beta/(1-beta)*(theta*y_2-i_2)``                    Price of claim on net output (country 1)
 .                   r_S_1             ``1/beta``                                           Expected return of claim on net output (country 1)
 .                   r_S_2             ``1/beta``                                           Expected return of claim on net output (country 1)
 .                   roc_1             ``1/beta``                                           Expected return on capital (country 1)
 .                   roc_2             ``1/beta``                                           Expected return on capital (country 2)
==================  ============  ======================================================= =====================================================



Equations of the model are entered using dolo conventions. A variable :math:`v_t` is simply denoted by ``v`` while :math:`v_{t+1}` and :math:`v_{t-1}` are denoted by ``v(1)`` and ``v(-1)`` respectively. By convention, all equations are surrounded by :math:`E_t \left[ \right]`. According to this rule, the symbolic expression ```x(1)=(1-x)`` means :math:`\forall t,  E_t \left[ x_{t+1}\right]=1+x`` while ``x = (1+x(-1))`` simply means :math:`\forall t,  x_t=1+x(-1)`.

Some notational shortcuts are defined in the ``definitions:`` section of the model file. These definitions are substituted directly into the other equations. Contrary to auxiliary variables, there is no restriction on the timing of these definitions. Table Y contains a list of all such definitions.

===========  =============================================================================================================  ==========================================
Variable       Definition                                                                                                   Description
===========  =============================================================================================================  ==========================================
r_I_1         ``theta*(y_1(1)/k_1(1))*Phi_1__p + Phi_1__p/Phi_1__p(1)*( (1-delta) + Phi_1(1)-i_1(1)/k_1(1)*Phi_1__p(1))``    (stochastic) Return on investment (1)
r_I_2         ``theta*(y_2(1)/k_2(1))*Phi_2__p + Phi_2__p/Phi_2__p(1)*( (1-delta) + Phi_2(1)-i_2(1)/k_2(1)*Phi_2__p(1))``    (stochastic) Return on investment (2)
rho_1         ``beta*( exp(E(1))*w_1(1)/ew_1 )**(psi-gamma) * (exp(E(1))*c_1(1)/c_1)**(-psi)``                               Stochastic discount factor (1)
rho_2         ``beta*( exp(E(1))*w_2(1)/ew_2 )**(psi-gamma) * (exp(E(1))*c_2(1)/c_2)**(-psi)``                               Stochastic discount factor (2)
lam           ``( db_f - min_b) / (max_b - min_b)``                                                                          Distance to debt boundaries (0<=lam<=1)
===========  =============================================================================================================  ==========================================


The various type of equations are defined in the ``equations:`` section, each type in its own subsection. Table Z exposes the full list of equations.

=============== ================================================================ ===============================================
Equation group  Equation                                                         Description
=============== ================================================================ ===============================================
transition      ``k_1 = ( (1-delta) + Phi_1(-1) )* k_1(-1) / exp(E)``               Capital law of motion in country 1
     .          ``k_2 = ( (1-delta) + Phi_2(-1) )* k_2(-1) / exp(E)``               Capital law of motion in country 2
     .          ``b_f = db_f(-1) / exp(E)``                                         Debt law of motion
--------------- ---------------------------------------------------------------- -----------------------------------------------
auxiliary       ``y_1 = k_1**theta*exp(A_1)``                                       Output (1)
     .          ``y_2 = k_2**theta*exp(A_2)``                                       Output (2)
     .          ``c_1 = y_1 - i_1 +  b_f - p_f*db_f``                               Consumption of country (1)
     .          ``c_2 = y_2 - i_2 - (b_f - p_f*db_f)/S_2*S_1``                      Consumption of country (2)
     .          ``Phi_1 = a1 + a2/(1-xi)*(i_1/k_1)^(1-xi)``                         Adjustment cost (1)
     .          ``Phi_2 = a1 + a2/(1-xi)*(i_2/k_2)^(1-xi)``                         Adjustment cost (2)
     .          ``Phi_1__p =  a2*(i_1/k_1)^(-xi)``                                  Derivative of adjustment cost (1)
     .          ``Phi_2__p =  a2*(i_2/k_2)^(-xi)``                                  Derivative of adjustment cost (2)
--------------- ---------------------------------------------------------------- -----------------------------------------------
arbitrage       ``rho_1 - rho_2  |   min_bb <= db_f <= max_bb``                     Price agreement    [#]_
     .          ``p_f = lam*rho_1 + (1-lam)*rho_2``                                 Pricing of the bond  [#]_
     .          ``rho_1 * r_I_1 - 1                     |   0.00 <= i_1 <= inf``    Optimal investment (1)
     .          ``rho_2 * r_I_2 - 1                     |   0.00 <= i_2 <= inf``    Optimal investment (2)
     .          ``1 = (  w_2(1)/ew_2*exp(E(1)) )**(1-gamma)``                       Definition of *quasi-expected* utility (1)
     .          ``1 = (  w_1(1)/ew_1*exp(E(1)) )**(1-gamma)``                       Definition of *quasi-expected* utility (2)
     .          ``beta - (w_1/ew_1)**(1-psi) + (1-beta)*(c_1/ew_1)**(1-psi)``       Recursive definition of utility (1)
     .          ``beta - (w_2/ew_2)**(1-psi) + (1-beta)*(c_2/ew_2)**(1-psi)``       Recursive definition of utility (2)
--------------- ---------------------------------------------------------------- -----------------------------------------------
value           ``roc_1 = r_I_1``                                                 Expected return on capital (1) [#]_
     .          ``roc_2 = r_I_2``                                                 Expected return on capital (2)
     .          ``p_S_1 = rho_1 * (p_S_1(1) + theta*y_1(1)-i_1(1) )*exp(E(1))``   Price of a claim on output (1)
     .          ``p_S_2 = rho_2 * (p_S_2(1) + theta*y_2(1)-i_2(1) )*exp(E(1))``   Price of a claim on output (2)
     .          ``r_S_1 = exp(E(1))*(p_S_1(1) + theta*y_1(1)-i_1(1) )/p_S_1``     Expected return for a claim on output (1)
     .          ``r_S_2 = exp(E(1))*(p_S_2(1) + theta*y_2(1)-i_2(1) )/p_S_2``     Expected return for a claim on output (2)
=============== ================================================================ ===============================================

.. [#] The complementarity condition  ``min_bb <= db_f <= max_bb`` reads as follows. If ``rho_1<rho_2`` (i.e country 1 wants to save more than country 2 wants to borrow) then ``min_bb = db_f`` (that is country 2 is credit constrained). Conversely, if ``rho_1>rho_2``, then ``max_bb = db_f`` (credit 1 is credit constrained).

.. [#] By definition of ``lam``, when country 1 is contrained, then the bond is priced using stochastic discount factor of country 2. Conversely, when country 2 is constrained, the bond is priced by country 1.

.. [#] In the equations defining of the return on capital ``roc_1`` (resp ``roc_2``), note that ``r_I_1`` (resp ``r_I_2``) are definitions which depend on variables at date :math:`t+1`

Solution algorithm
~~~~~~~~~~~~~~~~~~

The solution to the model is defined as a function of exogenous shocks and of endogenous state variables. The exogenous shocks consist in the productivity processes :math:`A_{1,t}` and :math:`A_{2,t}` as well as the permanent component of productivity :math:`E_t`. The continuous states are the levels of capital :math:`k_{1,t}` and :math:`k_{2,t}` as well as debt inherited from last period :math:`b_t` (in the incomplete markets case).

The walrasian equilibrium of the model consists in a price function :math:`p()` and a quantity :math:`db()` such that given :math:`p`, quantity :math:`db` maximizes utility of countries 1 and 2 (i.e., it is the desired amount of savings for country 1, and the optimal amount of borrowing from country 2). The first order conditions ensure that this condition is satisfied.


The Coleman iteration algorithm consists in taking a guess for a policy function at :math:`t+1` and use it to solve for the policy rules at :math:`t` which satisfy the Euler equations. As Kubler, ..., we extend this algorithm in a case where iterations don't correspond to a straight value function iteration (i.e. the updating rule corresponding the the Bellman equation for one agent). Here a pure value function iteration would take price as given and update the optimal :math:`b` for only one of the two agents, or give a different value for each agent. So instead, :math:`b()` and :math:`p()` are updated at each iteration together with :math:`i_1` and :math:`i_2` and required to satisfy the Euler equation for the two agents, implying that market for bonds clears at every step. If this algorithm converges, then the solution satisfies the first order conditions which characterize the walrasian equilibrium. Unfortunately there is not theoretical proof that there is indeed convergence.  Instead we must then watch the empirical convergence of the approximated iterations to get some kind of "computational" proof of existence and local unicity. The main criterion there are the convergence of the successive approximation algorithm, and whether the ratio of the successive errors is bounded by a constant strictly smaller than :math:`1`.

Consistency and accuracy checks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Because we are interested in welfare gains, it is essential to perform accuracy measures. This is also important in light of the fact that we don't have theoretical proof of existence of the solutions.

First, we inspect visually the approximated welfare: the fluctuations clearly establish that without enough grid points (10x10x10) the errors on the approximated welfares are of the same order of magnitude as the welfare gains, which makes any comparison spurious.

To check that the welfare is not impacted by the choice of the state-space, we change the boundaries for the debt and remove the borrowing constraint to see which impact both have on the initial welfare evaluations. [note that in order to avoid singularities, we need to reduce the boundaries for capital when we increase the boundaries for debt]

A good indication of whether the state-space is sufficiently big consists in looking at the number of simulations which go past the boundaries or hit the borrowing constraints. Due to the discrete approximation of productivity, capital crosses the frontiers very seldom. Because of the stabilising effect of risk-aversion, there is a mean-reverting force for debt, which prevent many simulations from hitting the constraint (see ergodic distribution).
We report the fraction of simulations, out of [Z] draws which hit the constraint before X periods. [maybe report it for any X ?]

As a more formal measure of accuracy, we also check the residual of the Euler equations, both in cross-sections [see graphs] and by using the a finer grid than the one used for collocations. We report the maximum errors on this grid as well as errors weighted by ergodic distributions.

These statistics, for all the calibrations are reported in the table below [and included in Excel file Z]


+-----------------+--------------------+-----------------------------+---------------------+------------------------+
|  Calibration    |   Figure number    | Constrained simulations     |    Maximum error    |   Weighted  Average    |
+-----------------+--------------------+-----------------------------+---------------------+------------------------+
| baseline_10     |                    |                             |                     |                        |
| baseline_30     |                    |                             |                     |                        |
+-----------------+--------------------+-----------------------------+---------------------+------------------------+


Instructions
------------

This section exposes how one version of the model is solved. The corresponding code is contained in notebook: notebooks/solution_example.ipynb




Solving one calibration of the model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


 
.. code:: python

    cd ..


.. parsed-literal::

    /home/pablo/Programming/papers


.. code:: python

    from dolo import yaml_import, pcat, display

.. code:: python

    
    model = yaml_import("models/integration_B.yaml")
    model


.. parsed-literal::

    Model type detected as 'dtmscc'




.. parsed-literal::

    
    Model object:
    ------------
    
    - name: "Partial Integration"
    - type: "dtmscc"
    - file: "models/integration_B.yaml
    
    - residuals:
    
        transition
            1   : 0.0000 : k_1 =        ( (1-delta) + Phi_1(-1) )*k_1(-1) / exp(E)
            2   : 0.0000 : k_2 =        ( (1-delta) + Phi_2(-1) )*k_2(-1) / exp(E)
            3   : 0.0000 : b_f = db_f(-1) / exp(E)
    
        arbitrage
            1   : 0.0000 : rho_1 - rho_2  |   min_bb <= db_f <= max_bb
            2   : 0.0000 : p_f = lam*rho_1 + (1-lam)*rho_2
            3   : 0.0000 : rho_1 * r_I_1 - 1                     |   0.00 <= i_1 <= inf
            4   : 0.0000 : rho_2 * r_I_2 - 1                     |   0.00 <= i_2 <= inf
            5   : 0.0000 : 1 = (  w_1(1)/ew_1*exp(E(1)) )**(1-gamma)
            6   : 0.0000 : 1 = (  w_2(1)/ew_2*exp(E(1)) )**(1-gamma)
            7   : 0.0000 : beta - (w_1/ew_1)**(1-psi) + (1-beta)*(c_1/ew_1)**(1-psi)
            8   : 0.0000 : beta - (w_2/ew_2)**(1-psi) + (1-beta)*(c_2/ew_2)**(1-psi)
    
        auxiliary
            1   : 0.0000 : y_1 = k_1**theta*exp(A_1)
            2   : 0.0000 : y_2 = k_2**theta*exp(A_2)
            3   : 0.0000 : c_1 = y_1 - i_1 +  b_f - p_f*db_f
            4   : 0.0000 : c_2 = y_2 - i_2 - (b_f - p_f*db_f)/S_2*S_1
            5   : 0.0000 : Phi_1 = a1 + a2/(1-xi)*(i_1/k_1)**(1-xi)
            6   : 0.0000 : Phi_2 = a1 + a2/(1-xi)*(i_2/k_2)**(1-xi)
            7   : 0.0000 : Phi_1__p =  a2*(i_1/k_1)**(-xi)
            8   : 0.0000 : Phi_2__p =  a2*(i_2/k_2)**(-xi)
    
        value
            1   : 0.0000 : roc_1 = r_I_1
            2   : 0.0000 : roc_2 = r_I_2
            3   : 0.0000 : p_S_1 = rho_1 * (p_S_1(1) + theta*y_1(1)-i_1(1) )*exp(E(1))
            4   : 0.0000 : p_S_2 = rho_2 * (p_S_2(1) + theta*y_2(1)-i_2(1) )*exp(E(1))
            5   : 0.0000 : r_S_1 = exp(E(1))*(p_S_1(1) + theta*y_1(1)-i_1(1) )/p_S_1
            6   : 0.0000 : r_S_2 = exp(E(1))*(p_S_2(1) + theta*y_2(1)-i_2(1) )/p_S_2




.. code:: python

    model.set_calibration(N_k=10, N_b=10)
    model.set_calibration(n_a=2)             # set the number of values for the productivity process

discretization of the markov variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: python

    P, Q = model.markov_chain


Each line of ``P`` represents one value for the vector ``(A_1, A_2, E)``

.. code:: python

    display(P)
    display('Shape: {}'.format(P.shape))



.. parsed-literal::

    array([[-0.05735393, -0.11470787,  0.        ],
           [-0.05735393,  0.11470787,  0.        ],
           [ 0.05735393, -0.11470787,  0.        ],
           [ 0.05735393,  0.11470787,  0.        ]])



.. parsed-literal::

    'Shape: (4, 3)'


We can see that there are ``N_m=4`` different values for the markov
process. The transition matrix ``Q`` is such that for any ``i,j``, the
cell ``Q[i,j]`` is the probability of jumping from state ``i`` to state
``j``.

.. code:: python

    display(Q)
    display('Sum of each row: {}'.format(Q.sum(axis=1)))



.. parsed-literal::

    array([[ 0.9025,  0.0475,  0.0475,  0.0025],
           [ 0.0475,  0.9025,  0.0025,  0.0475],
           [ 0.0475,  0.0025,  0.9025,  0.0475],
           [ 0.0025,  0.0475,  0.0475,  0.9025]])



.. parsed-literal::

    'Sum of each row: [ 1.  1.  1.  1.]'


state-space for the continuous variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The data defining the continuous state-space is contained in the
``model.options['approximation_space']`` field

.. code:: python

    ap = model.options['approximation_space']
    display(ap)



.. parsed-literal::

    {'a': [2, 2, -10.0], 'b': [10, 10, 10.0], 'orders': [10, 10, 10]}


It contains the lower- and upper-bounds (keys ``a`` and ``b``) for the
vector of continuous states ``(k_1, k_2, b_f)``. The number of nodes
along each dimension is defined by key ``orders``. The cartesian grid is
represented as a ``Nxn_s`` grid, where ``N=10*10*10`` is the number of
points of the cartesian product and ``n_x=3`` the number of states. Each
line corresponds to a different point in the state-space. The cartesian
product operates on the last index first.

.. code:: python

    from dolo.numeric.misc import mlinspace
    grid = mlinspace(ap['a'], ap['b'], ap['orders'])
    print('Size of the grid {}'.format(grid.shape))
    print('Columns: (k_1, k_2, b_f)')
    print("First 15 elements: ")
    print(grid[:15,:])



.. parsed-literal::

    Size of the grid (1000, 3)
    Columns: (k_1, k_2, b_f)
    First 15 elements: 
    [[  2.           2.         -10.        ]
     [  2.           2.          -7.77777778]
     [  2.           2.          -5.55555556]
     [  2.           2.          -3.33333333]
     [  2.           2.          -1.11111111]
     [  2.           2.           1.11111111]
     [  2.           2.           3.33333333]
     [  2.           2.           5.55555556]
     [  2.           2.           7.77777778]
     [  2.           2.          10.        ]
     [  2.           2.88888889 -10.        ]
     [  2.           2.88888889  -7.77777778]
     [  2.           2.88888889  -5.55555556]
     [  2.           2.88888889  -3.33333333]
     [  2.           2.88888889  -1.11111111]]


Defining the first order guess
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The solution to the model must be a function ``f(i,s)``. These arguments
are: - ``i``: the index of a markov state - ``s``: a list of ``M``
different states vectors by an ``Mx3`` matrix. The function must return
a list of ``M`` controls vectors as an ``Mxn_x`` vector (there are
``n_x=8`` controls)

In order to start the iteration process, some initial guess, for this
solution must be provided. The initial guess function is provided in the
file ``crw_code/make_initial_guess.py``.

.. code:: python

    from crw_code.make_initial_guess import initial_dr_int
    # other options are initial_dr_aut and initial_dr_comp for autarky and completemarkets respectively
    dri = lambda i,s: initial_dr_int(model, i, s) # constructs the function
    vals = dri(0, grid[:15,:])
    print("Values of the controls on the 15 first points of the grid: (shape {})".format(vals.shape))
    print(vals)


.. parsed-literal::

    Values of the controls on the 15 first points of the grid: (shape (15, 8))
    [[-10.           0.96         0.16         0.16         0.67114441
        1.47114441   0.67114441   1.47114441]
     [ -7.77777778   0.96         0.16         0.16         0.7600333
        1.38225552   0.7600333    1.38225552]
     [ -5.55555556   0.96         0.16         0.16         0.84892219
        1.29336664   0.84892219   1.29336664]
     [ -3.33333333   0.96         0.16         0.16         0.93781108
        1.20447775   0.93781108   1.20447775]
     [ -1.11111111   0.96         0.16         0.16         1.02669997
        1.11558886   1.02669997   1.11558886]
     [  1.11111111   0.96         0.16         0.16         1.11558886
        1.02669997   1.11558886   1.02669997]
     [  3.33333333   0.96         0.16         0.16         1.20447775
        0.93781108   1.20447775   0.93781108]
     [  5.55555556   0.96         0.16         0.16         1.29336664
        0.84892219   1.29336664   0.84892219]
     [  7.77777778   0.96         0.16         0.16         1.38225552
        0.7600333    1.38225552   0.7600333 ]
     [ 10.           0.96         0.16         0.16         1.47114441
        0.67114441   1.47114441   0.67114441]
     [-10.           0.96         0.16         0.23111111   0.67114441
        1.54362472   0.67114441   1.54362472]
     [ -7.77777778   0.96         0.16         0.23111111   0.7600333
        1.45473583   0.7600333    1.45473583]
     [ -5.55555556   0.96         0.16         0.23111111   0.84892219
        1.36584694   0.84892219   1.36584694]
     [ -3.33333333   0.96         0.16         0.23111111   0.93781108
        1.27695805   0.93781108   1.27695805]
     [ -1.11111111   0.96         0.16         0.23111111   1.02669997
        1.18806916   1.02669997   1.18806916]]


Solving the model
^^^^^^^^^^^^^^^^^

The actual solution code is written in the time\_iteration fuction. When
called with the ``verbose=True`` switch it produces the following output
reads as follows: - first column is the iteration number ``n`` at which
the matrix of controls :math:`X_n` is solved - second columns is the
distance :math:`X_n - X_{n-1}` - third column is the ratio
:math:`\frac{X_n - X_{n-1}}{X_{n-1} - X_{n-2}}` - fourth column is the
duration of each iteration - last column is the number of newton steps
taken to optimize the residuals (more informations with
``verbose='full'``) To limit the size of the output, we stop after 5
iterations.

.. code:: python

    from crw_code.time_iteration import time_iteration
    dr = time_iteration(model, initial_guess=dri, verbose=True, maxit=5)


.. parsed-literal::

    Solving WITH complementarities.
    ------------------------------------------------
    | N   |  Error     | Gain     | Time     | nit |
    ------------------------------------------------
    |   1 |  6.604e-01 |      nan |    6.597 |  10 |
    |   2 |  2.077e-01 |    0.314 |    4.435 |   8 |
    |   3 |  1.527e-01 |    0.735 |    4.148 |   8 |
    |   4 |  1.154e-01 |    0.756 |    4.124 |   9 |
    |   5 |  8.080e-02 |    0.700 |    3.698 |   8 |
    ------------------------------------------------
    Elapsed: 23.0045042038 seconds.
    ------------------------------------------------


The object returned by the ``time_iteration`` function is a decision
rule object, which can be used to compute controls at any state:

.. code:: python

    vals = dr(0, grid[:15,:])
    print('shape : {}'.format(vals.shape))


.. parsed-literal::

    shape : (15, 8)


In particular, it can be used as an initial guess for a different
calibration or a modfied state-space. Here we finish the iterations that
were previously stopped after 5 rounds:

.. code:: python

    dr = time_iteration(model, initial_guess=dr, verbose=False)


.. parsed-literal::

    Solving WITH complementarities.


Simulations
^^^^^^^^^^^


Accuracy checks
^^^^^^^^^^^^^^^





Solving many calibrations in parallel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Given the time it takes to solve fully one version of the model, it is useful to execute these computations in parallel. The execution order and the dependencies between various calibrations are defined ``yaml`` files, while the code responsible for managing the computational tasks lies in ``name-of-the-file``.

A calibration file contain different sets of parameter values to solve a given model file, solves all these calibrations in turn and stores the results in a file. The file ``models/calibration_normal.yaml`` lists all calibrations used in the paper, save for the long-run risk cases that are defined in ``calibration_long_run.yaml``.

Calibration files are run using the ``run_calibrations.py`` command supplying a calibration file as an argument (see ``run_calibrations.py --help`` for details).

The model file and where to store the ouptut is specified in the ``options:`` section of the file:

.. code:: yaml
  :number-lines: 2

    options:
        max_cpus: 4
        model: models/integration_B
        store: results/normal_B
        refine: True

These options can be overridden when running the calibration file. For
instance:

::

    python run_calibrations.py models/calibration_normal.yaml --model=models/integration_A --store=results/solutions_A --max_cpus=2

will use the autarky model specified in ``models/integration_A.yaml`` and store the resulting decision rules in ``results/normal_A`` using 2 cpus instead of 4.

Individual calibrations are specified in the ``tasks`` section. Each calibration is defined by a name and a list of parameter values. These parameter values then replace the calibration defined in the model files. For instance, the baseline calibration with a smaller number of points (10x10x10 instead of 30x30x30) is named ``baseline_10`` and specified as:

.. code:: yaml
  :number-lines: 9

    tasks:

        - name: baseline_10
          content:
              n_a: 3
              n_e: 1
              n_u: 1
              xi: 0.2
              psi: 4.0
              gamma: 4.0
              sigma_E: 0.00
              delta: 0.08
              min_b: -10.0     # should be -10 for consistency
              max_b: 10.0
              min_bb: min_b
              max_bb: max_b
              N_k: 10
              N_s: 10
              N_b: 10
              kmin: 1.5
              kmax: 10


Calibration can also depend upon each other. In this case, unspecified parameters are taken from the parent calibration. In the following example, the calibration ``baseline_30`` has the same parameters as the baseline, but uses more grid points, specified by parameters ``N_k``, ``N_s`` and ``N_b``:

.. code:: yaml
  :number-lines: 124

        - name: baseline_30
          parent: baseline_10
          content:
              N_k: 30
              N_s: 30
              N_b: 30


When a calibration is depends on another one, the solution of the parent is used as an initial guess. When more than 1 cpus are used, a parallel scheduler ensures that parent calibrations are solved first. Here are the dependency graphs for the calibrations used in the paper:


.. raw:: html

  <!--

.. raw:: latex

  \includegraphics[width=\textwidth]{results/calibration_normal.pdf}
  \includegraphics[width=\textwidth]{results/calibration_long_run.pdf}

.. raw:: html

  -->

.. raw:: latex

  \iffalse


.. image:: results/calibration_normal.svg


.. image:: calibration_long_run.svg
  :scale: 100

.. raw:: latex

    \fi