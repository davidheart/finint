from __future__ import print_function

def parallel_run(fun, tasks, max_cpus=4, store=None, refine=False):

    import copy

    if store is None:
        raise Exception("Unspecified store file.")

    import h5py
    store = h5py.File(store)

    def save_results(h5f,sol,name):

        if name in h5f:
            del h5f[name]

        scenario = name

        for name in ['solution', 'values']:
            dr = sol[name]
            h5f.create_dataset('{}/{}/values'.format(scenario, name), data=dr.__values__)
            h5f.create_dataset('{}/{}/a'.format(scenario, name), data=dr.a)
            h5f.create_dataset('{}/{}/b'.format(scenario, name), data=dr.b)
            h5f.create_dataset('{}/{}/orders'.format(scenario, name), data=dr.orders)

        import numpy
        symbolic_calibration = numpy.array( [(str(e[0]).encode('utf8'), str(e[1]).encode('utf8')) for e in sol['calib'].items()] )
        numeric_calibration = numpy.array( [(str(e[0]).encode('utf8'), float(e[1])) for e in sol['calibration_dict'].items()] )

        h5f.create_dataset( '{}/calibration/symbolic'.format(scenario), data=symbolic_calibration)
        h5f.create_dataset( '{}/calibration/numeric'.format(scenario), data=numeric_calibration)

    def load_results(h5f,name):
        scenario = name
        sol = dict()
        import numpy
        for name in ['solution', 'values']:
            values = h5f['{}/{}/values'.format(scenario, name)]
            a = h5f['{}/{}/a'.format(scenario, name)]
            b = h5f['{}/{}/b'.format(scenario, name)]
            orders = h5f['{}/{}/orders'.format(scenario, name)]
            from dolo.numeric.decision_rules_markov import MarkovDecisionRule
            [a,b,orders,values] = [numpy.array(t) for t in [a,b,orders,values]]
            mdr = MarkovDecisionRule(values.shape[0], a,b, orders)
            mdr.set_values(values)

            sol[name] = mdr

        return sol

    task_names = [t['name'] for t in tasks]

    running_processes = dict()
    remaining_tasks = [t for t in tasks]
    finished_tasks = []
    failed = []

    import os
    import time


    from ipyparallel import Client
    rc = Client()
    print(rc.ids)
    if max_cpus < len(rc.ids):
        pool = rc[:max_cpus]
    else:
        pool = rc.load_balanced_view()
        max_cpus = len(rc.ids)

    print("Using {} cpu(s)".format(max_cpus))

    import subprocess
    try:
        cmd_output = subprocess.check_output('tmux list-panes', shell=True)
    except Exception as e:

    # if isinstance(cmd_output, tuple) and 'failed' in cmd_output[1]:
        tmux_output = False
        print("TMUX is not running")
    else:
        tmux_output = True
        n_panes = int(cmd_output[1])
        if n_panes>1:
            os.system("tmux kill-pane -a")
        for i in range(max_cpus):
            os.system("touch log_{}".format(i))
            os.system('tmux split-window "tail -f log_{}"'.format(i))
            os.system("tmux select-layout tiled")
        os.system("tmux select-pane -t 0")

    def get_children(s):
        if isinstance(s,list):
            return [e['name'] for e in tasks if 'parent' not in e]
        else:
            return [e['name'] for e in tasks if e.get('parent')==s]

    def print_node(s):
        if isinstance(s, list):
            return u''
        else:
            if s in finished_tasks:
                return u'{} \u2713'.format(s)
            elif s in failed:
                return u'{} \u2717'.format(s)
            elif s in running_processes.keys():
                return u'{} (running)'.format(s)
            else:
                return s
    from unicodetree import draw_tree

    def summarize_state():

        txt = draw_tree(tasks, child_iter=get_children, text_str=print_node)
        N = max( [len(e) for e in txt.split('\n') ] )
        print("="*N)
        print(txt)
        print("="*N)

    def print_process(p, txt=None):

        n = p.output_length
        ind = p.ind
        if isinstance(p.stdout, list):
            sout = p.stdout[0]
        else:
            sout = p.stdout
        if txt is None and len(sout)>n:
            txt = sout[n:]
            p.output_length += len(txt)
        if txt is not None:
            if not tmux_output:
                print(txt)
                # return
            with open('log_{}'.format(ind),'a') as f:
                f.write(txt)


    logfiles = []
    finished = False


    summarize_state()

    it = 0

    while not finished:

        it += 1

        # check whether running tasks have finished
        # if so store results and remove task
        for name, p in list(running_processes.items()):

            if p.ready():
                print("Task {} is ready".format(name))
                res = p.get()
                # why ?
                # balanced view does not behave in the same way ?
                if isinstance(res, list):
                    res = res[0]
                # storing results
                save_results(store, res, name)
                # results[name] = res
                # if store is not None:
                #     results.sync()

                ind = p.ind
                print('removing {}'.format(ind))
                print_process(p, txt="Finished task\n")
                logfiles.remove(ind)

                running_processes.pop(name)
                finished_tasks.append(name)
                if len(running_processes) == 0 and len(remaining_tasks) == 0:
                    finished = True

            print_process(p)

        for t in remaining_tasks:

            name = t['name']
            parent = t.get('parent')

            ready = (parent is None or parent in finished_tasks)

            if len(running_processes) < max_cpus and ready:

                if parent is not None:
                    parent_task = tasks[task_names.index(parent)]
                    args = copy.copy(parent_task['content'])
                    args.update(t['content'])
                    #print("Updated content: {}".format(args))
                    t['content'] = args
                else:
                    pass
                    #print("Not updated content: {}".format(t['content']))

                if refine and name in store:
                    guess = load_results(store, name)
                    guess = copy.deepcopy(guess)
                elif parent is not None:
                    guess = load_results(store, parent)
                else:
                    guess = None

                # FIX
                if isinstance(guess, list):
                    guess = guess[0]


                kwargs = {'start': guess}
                kwargs.update(t['content'])

                print("Launching : {}".format(name))
                print(kwargs)
                p = pool.apply_async(fun, **kwargs)



                ind = [i for i in range(max_cpus) if i not in logfiles][0]
                p.ind = ind
                p.output_length = 0

                print_process(p, txt="Started : {}\n".format(name))
                running_processes[name] = p

                logfiles.append(ind)
                remaining_tasks.remove(t)

                summarize_state()

        time.sleep(1)

    store.close()
    #
    # print("Store: {}".format(store))
    # if store is not None:
    #     results.sync()
    #     results.close()
    #     return None
    # else:
    #     return results

def run_taskfile(fun, task_file):

    import yaml

    if isinstance(task_file, str):
        with open(task_file) as f:
            txt = f.read()
    else:
        txt = task_file

    task_dict = yaml.load(txt)

    options = task_dict['options']

    tasks = task_dict['tasks']

    results = parallel_run(fun, tasks, **options)

    return results


def get_full_tasks(task_file):

    from collections import OrderedDict
    import copy

    import yaml

    if isinstance(task_file, str):
        with open(task_file) as f:
            txt = f.read()
    else:
        txt = task_file

    calib_struct = yaml.load(txt)

#    options = task_dict['options']
#    tasks = task_dict['tasks']

    full_tasks = OrderedDict()
    small_tasks = OrderedDict( {t['name']:t for t in calib_struct['tasks'] } )

    it = 0

    while len(small_tasks)>1 and it<50:
        it += 1
        for t in small_tasks.values():
            parent_name = t.get('parent')
            if parent_name is None:
                full_tasks[t['name']] = copy.deepcopy(t['content'])
                small_tasks.pop(t['name'])
            elif parent_name in full_tasks:
                cont = copy.deepcopy(t['content'])
                cont.update(full_tasks[parent_name])
                full_tasks[t['name']] = cont
    return full_tasks

if __name__ == '__main__':

    txt = '''

options:

    max_cpus: 5
#    store: outfile
#    refine: true

tasks:

    - name: baseline
      content:
        a: 1
        b: 2
        c: 3

    - name: high_a
      parent: baseline
      content:
        a: 5

    - name: very_high_a
      parent: high_a
      content:
        a: 9

    - name: high_b
      parent: baseline
      content:
        b: 5

    '''

    from scheduler_lib import fun
    import yaml


    task_dict = yaml.load(txt)

    options = task_dict['options']
    tasks = task_dict['tasks']

    results = parallel_run(fun, tasks, **options)

    print('results')
    print(results)
