from dolo import *

import numpy


def initial_dr_aut(model, i, s):

    beta, delta, theta, psi, gamma, a1, a2, xi = model.get_calibration(['beta', 'delta', 'theta', 'psi', 'gamma', 'a1', 'a2', 'xi'])

    k_bar = model.calibration['states'][0]

    k_1 = s[:,0]

    i_1 = k_1*delta
#    i_1 = k_bar*delta + (k_1-k_bar)*(delta

    N = s.shape[0]
    val = numpy.zeros( (N,3))

    val[:,0] = i_1 #[None,:]

    c_1 = k_1**theta - i_1
    val[:,1] = c_1
    val[:,2] = c_1

    return val

def initial_dr_aut_2(model, i, s):

    beta, delta, theta, psi, gamma, a1, a2, xi = model.get_calibration(['beta', 'delta', 'theta', 'psi', 'gamma', 'a1', 'a2', 'xi'])

    k_1 = s[:,0]
#    sparsity = s[:,1]
#    k_2 = k_1*sparsity
    k_2 = s[:,1]
    i_1 = k_1*delta
    i_2 = k_2*delta

    N = s.shape[0]
    val = numpy.zeros( (N,6))

    val[:,0] = i_1 #[None,:]
    val[:,1] = i_2 #[None,:]

    c_1 = k_1**theta - i_1
    c_2 = k_2**theta - i_2
    val[:,2] = c_1
    val[:,3] = c_2
    val[:,4] = c_1
    val[:,5] = c_2

    return val


def initial_dr_int(model, i, s):

    from numpy import arctan
    from numpy import pi

    beta, delta, theta, psi, gamma, a1, a2, xi, S_1, S_2 = model.get_calibration(['beta', 'delta', 'theta', 'psi', 'gamma', 'a1', 'a2', 'xi', 'S_1', 'S_2'])

    k_1 = s[:,0]
    #sparsity = s[:,1]
    #k_2 = k_1*sparsity
    k_2 = s[:,1]
#    b_f = arctan( s[:,2] )/pi*2*0.8
    b_f = s[:,2]

    i_1 = k_1*delta
    i_2 = k_2*delta
    b   = b_f
    p = b_f*0 + beta

    N = s.shape[0]
    val = numpy.zeros( (N,8))

    val[:,0] = b #[None,:]
    val[:,1] = p #[None,:]
    val[:,2] = i_1 #[None,:]
    val[:,3] = i_2 #[None,:]

    c_1 = k_1**theta - i_1 + b*(1-p)
    c_2 = k_2**theta - i_2 - b*(1-p)/S_2*S_1
    val[:,4] = c_1
    val[:,5] = c_2
    val[:,6] = c_1
    val[:,7] = c_2

    return val


def initial_dr_int_t(model, i, s):

    from numpy import arctan, exp
    from numpy import pi

    beta, delta, theta, psi, gamma, a1, a2, xi, min_b, max_b, fac, theta_f = model.get_calibration(['beta', 'delta', 'theta', 'psi', 'gamma', 'a1', 'a2', 'xi', 'min_b', 'max_b', 'fac', 'theta_f'])

    k_1 = s[:,0]
    #sparsity = s[:,1]
    #k_2 = k_1*sparsity
    k_2 = s[:,1]
#    b_f = arctan( s[:,2] )/pi*2*0.8
    bl = s[:,2]

    A_1, A_2, E = model.markov_chain[0][i,:]

    min_c = -k_1**theta_f*fac #*0 + min_b
    max_c = k_2**theta_f*fac #*0 + max_b
    b_f = (bl+1)/2*max_c + (1-bl)/2*min_c

    i_1 = k_1*delta
    i_2 = k_2*delta
    b   = b_f
    p = b_f*0 + beta

    N = s.shape[0]
    val = numpy.zeros( (N,8))

    val[:,0] = b #[None,:]
    val[:,1] = p #[None,:]
    val[:,2] = i_1 #[None,:]
    val[:,3] = i_2 #[None,:]

    c_1 = exp(A_1)*k_1**theta - i_1 + b*(1-p)
    c_2 = exp(A_2)*k_2**theta - i_2 - b*(1-p)
    val[:,4] = c_1
    val[:,5] = c_2
    val[:,6] = c_1
    val[:,7] = c_2

    return val


def initial_dr_int_2(model, i, s):

    from numpy import arctan
    from numpy import pi

    beta, delta, theta, psi, gamma, a1, a2, xi, fac, min_b, max_b = model.get_calibration(['beta', 'delta', 'theta', 'psi', 'gamma', 'a1', 'a2', 'xi','fac', 'min_b', 'max_b'])

    k_1 = s[:,0]
    #sparsity = s[:,1]
    #k_2 = k_1*sparsity
    k_2 = s[:,1]
#    b_f = arctan( s[:,2] )/pi*2*0.8
    #b_f = s[:,2]
    bl = s[:,2]

    min_c = -k_1**theta*fac*0 + min_b
    max_c = k_2**theta*fac*0 + max_b
    b_f = min_c*(1-bl)/2 + max_c*(1+bl)/2

    i_1 = k_1*delta
    i_2 = k_2*delta
    b   = b_f
    p = b_f*0 + beta

    N = s.shape[0]
    val = numpy.zeros( (N,8))

    val[:,0] = b #[None,:]
    val[:,1] = p #[None,:]
    val[:,2] = i_1 #[None,:]
    val[:,3] = i_2 #[None,:]

    c_1 = k_1**theta - i_1 + b*(1-p)
    c_2 = k_2**theta - i_2 - b*(1-p)
    val[:,4] = c_1
    val[:,5] = c_2
    val[:,6] = c_1
    val[:,7] = c_2

    return val


def initial_dr_comp(model, i, s):

    beta, delta, theta, psi, gamma, a1, a2, xi = model.get_calibration(['beta', 'delta', 'theta', 'psi', 'gamma', 'a1', 'a2', 'xi'])

    k_1 = s[:,0]
#    sparsity = s[:,1]
#    k_2 = k_1*sparsity
    k_2 = s[:,1]
    i_1 = k_1*delta
    i_2 = k_2*delta

    N = s.shape[0]
    val = numpy.zeros( (N,4))

    val[:,0] = i_1 #[None,:]
    val[:,1] = i_2 #[None,:]

    C = k_1**theta  + k_2**theta - i_1 - i_2
    val[:,2] = C/2
    val[:,3] = C/2

    return val


if __name__ == '__main__':



    model.options['approximation_space']['orders'][:] = [10,10,10]
    print(model)
    print(model.markov_chain)

    def go():
        mdr = time_iteration.solve_mfg_model(model, maxit=5, verbose='full', initial_guess=initial_dr, with_complementarities=True)

    go()
