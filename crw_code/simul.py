from dolo.numeric.filters import hp_filter
from dolo.algos.dtmscc.simulations import simulate
import shelve
from dolo import *
import pandas


def compute_all_simulations(scenario, capital_scarcity=1, n_exp = 100, horizon = 150):

    from numpy import array
    import copy
    import yaml
    from collections import OrderedDict
    from dolo.algos.dtmscc.simulations import simulate
    
    import shelve
    

    calib_file = 'calibration_{}.yaml'.format(scenario)
#     if scenario == 'long_run':
#         solution_shelf = 'results/{}'.format(scenario)
#     else:
    solution_shelf = 'results/solutions'

    with file(calib_file) as f:
        calib_struct = yaml.load(f)

    full_tasks = OrderedDict()
    small_tasks = OrderedDict( {t['name']:t for t in calib_struct['tasks'] } )
    it = 0
    while len(small_tasks)>1 and it<50:
        it += 1
        for t in small_tasks.values():
            parent_name = t.get('parent')
            if parent_name is None:
                full_tasks[t['name']] = copy.deepcopy(t['content'])
                small_tasks.pop(t['name'])
            elif parent_name in full_tasks:
                cont = copy.deepcopy(t['content'])
                cont.update(full_tasks[parent_name])
                full_tasks[t['name']] = cont

    models = OrderedDict()
    for calib_name, calib in full_tasks.iteritems():
        model_A = yaml_import("models/integration_A.yaml", verbose=False)
        model_B = yaml_import("models/integration_B.yaml", verbose=False)
        model_C = yaml_import("models/integration_C.yaml", verbose=False)
        model_A.set_calibration(**calib)
        model_B.set_calibration(**calib)
        model_C.set_calibration(**calib)
        models[calib_name] = [model_A, model_B, model_C]
    


    # compute starting point



    for calib_name in full_tasks:

            print("Loading results: {}".format(calib_name))

            model_A,model_B,model_C = models[calib_name]
            sols = []
            for case in ['A', 'B', 'C']:
                sh = shelve.open(solution_shelf + '_' + case)
                sol = sh[calib_name]
                dr = sol['solution']
                drv = sol['values']
                sols.append([dr,drv])
                sh.close()
            #     sh.close()
            [[dr_A,drv_A], [dr_B, drv_B], [dr_C, drv_C]] = sols
            
            i0 = dr_A.n_m/2 
            
            markov_indices = array([i0]*horizon)
            
            sim_aut = simulate(model_A, dr_A, i0, n_exp=0, horizon=horizon, markov_indices=markov_indices)
            
            if scenario == 'long_run':
                s0_A = array( sim_aut[['k_1', 'k_2']].irow(-1) )
                s0_A[1] = capital_scarcity
            else:
                s0_A = array( sim_aut[['k_1', 'k_2']].irow(-1) )
                s0_A[1] = s0_A[0]*capital_scarcity
            s0_B = array([s0_A[0], s0_A[1], 0])
            s0_C = s0_A
            s0_ = [s0_A, s0_B, s0_C]
            
            # make irf

            simulations = []
            for i,case in enumerate(['A','B','C']):
                print('Simulating : {} ({})'.format(calib_name, case))
                model = models[calib_name][i]
                variables = model.variables
                dr = sols[i][0]
                drv = sols[i][1]
                if n_exp == 0:
                    panel = simulate(model, dr, i0, s0=s0_[i], horizon=horizon, drv=drv, n_exp=n_exp, markov_indices=markov_indices)  # n_exp
                else:
                    panel = simulate(model, dr, i0, s0=s0_[i], horizon=horizon, drv=drv, n_exp=n_exp)  # n_exp

                simulations.append(panel)
            
                
            all_simulations = shelve.open('results/all_simulations')
            all_simulations[str((scenario, calib_name, n_exp, capital_scarcity))] = simulations
            all_simulations.close()


#compute_all_simulations('long_run', n_exp=1000)
#compute_all_simulations('long_run', n_exp=1000, capital_scarcity=0.5)

#compute_all_simulations('long_run', n_exp=0, horizon=1000)
#compute_all_simulations('long_run', n_exp=0, capital_scarcity=0.5, horizon=1000)


def add_variables(simulations):
    from numpy import log
    import copy
    new_sims = []
    for i in range(3):  ## need to deal with complete markets differently
        sim = simulations[i]
        if sim.ndim == 3:
            ssim = sim.transpose(2,1,0)
        else:
            ssim = sim
#         ssim['vol_i_1_vol_y_1'] = ssim['i_1']/ssim['y_1']
#         ssim['vol_i_2_vol_y_2'] = ssim['i_2']/ssim['y_2']
        theta = (( log(ssim['y_1']) - ssim['A_1'] ) / log(ssim['k_1']) )[0]
        try:
            theta = float(theta)
        except:
            theta = float(theta[0])

        if i==2:
            tt = ssim['pwealth_1']/(ssim['pwealth_1'] + ssim['pwealth_2'])
            if sim.ndim == 3:
                sh = numpy.array( tt[0][:,None] ).repeat(tt.shape[1], axis=1)
            else:
                sh = numpy.array( tt )[0]
            ssim['sh_C'] = sh
            ssim['c_1'] = ssim['C']*ssim['sh_C']
            ssim['c_2'] = ssim['C']*(1-ssim['sh_C'])
            
        if i==1:
            ssim['r_f'] = 1/ssim['p_f']
            ssim['debt/gdp'] = ssim['b_f']/ssim['y_1']
            # compute excess returns
            p_S_1 = ssim['p_S_1']
            p_S_2 = ssim['p_S_2']
            i_1 = ssim['i_1']
            i_2 = ssim['i_2']
            y_1 = ssim['y_1']
            y_2 = ssim['y_2']
            r_f = 1/ssim['p_f']
            try:
                r_x_1 = (p_S_1 + (y_1*theta) - i_1)/(p_S_1.shift(1))  - r_f.shift(1)
                r_x_2 = (p_S_2 + (y_2*theta) - i_2)/(p_S_2.shift(1))  - r_f.shift(1)
            except:
                r_x_1 = (p_S_1 + (y_1*theta) - i_1)/(p_S_1.shift(1))  - r_f.shift(1)
                r_x_2 = (p_S_2 + (y_2*theta) - i_2)/(p_S_2.shift(1))  - r_f.shift(1)
            ssim['r_x_1'] = r_x_1
            ssim['r_x_2'] = r_x_2
        if i==0:
            ssim['r_f_1'] = 1/ssim['p_1']
            ssim['r_f_2'] = 1/ssim['p_2']
             # compute excess returns
            p_S_1 = ssim['p_S_1']
            p_S_2 = ssim['p_S_2']
            i_1 = ssim['i_1']
            i_2 = ssim['i_2']
            y_1 = ssim['y_1']
            y_2 = ssim['y_2']
            r_f_1 = 1/ssim['p_1']
            r_f_2 = 1/ssim['p_2']
            try:
                r_x_1 = (p_S_1 + (y_1*theta) - i_1)/(p_S_1.shift(1))  - r_f_1.shift(1)
                r_x_2 = (p_S_2 + (y_2*theta) - i_2)/(p_S_2.shift(1))  - r_f_2.shift(1)
            except:
                r_x_1 = (p_S_1 + (y_1*theta) - i_1)/(p_S_1.shift(1))  - r_f_1.shift(1)
                r_x_2 = (p_S_2 + (y_2*theta) - i_2)/(p_S_2.shift(1))  - r_f_2.shift(1)
            ssim['r_x_1'] = r_x_1
            ssim['r_x_2'] = r_x_2
        ssim['NX'] = (ssim['y_1'] - ssim['c_1'] - ssim['i_1'] )  # CA
        ssim['NX/y'] = (ssim['y_1'] - ssim['c_1'] - ssim['i_1'] )/ssim['y_1']  # CA
        if sim.ndim == 3:
            sim = ssim.transpose(2,1,0)
        else:
            sim = ssim
        new_sims.append(sim)
    return new_sims

## In[11]:
#
#sh = shelve.open("results/all_simulations")
#for k in ( sh.keys() ):
#    if 'long_run' in k or "'baseline_ez'" in k:
#        print(k)
#sh.close()
#
#
## In[9]:
#
#cd finint
#
#
## In[10]:
#
#import shelve
#
#
## In[9]:
#
# update simulations
#sh = shelve.open('results/all_simulations')
#for k in sh.keys():
#    if 'long_run' in k or "'baseline_ez'" in k:
#        print(k)
#        sim = sh[k]
#        nsim = add_variables(sim)
#        sh[k] = nsim
#sh.close()


# In[13]:


# In[ ]:

# import shelve
# sh = shelve.open("results/all_simulations")
# sim = sh[sh.keys()[0]]
# sh.close()


# # Compute statistics

# In[10]:

import statsmodels as sm
import statsmodels.api as sm
from collections import OrderedDict


# In[11]:

all_stds = OrderedDict()
all_corrs = OrderedDict()


# In[19]:

from numpy import log


horiz = 50


def get_statistics(scenario):

    all_simulations = shelve.open('results/all_simulations')

    from __builtin__ import sum
    
    for vv in sorted(all_simulations.keys()):
        
        
        calib_name = vv #all_simulations[vv]
        
        print(calib_name)
        scenario_, calibration, n_exp, scarsity = eval(calib_name)
        
        if scenario_ != scenario or scarsity != 1 or n_exp == 0:
            continue

        sims = all_simulations[calib_name]
        case_stats_stds = []
        case_stats_corrs = []
        for i_c,sim in enumerate(sims):
            stats_stds = []
            stats_corrs = []
            for k in range(sim.shape[0]):
                one_sim = sim[k]

    #             if i_c == 1:
    #                  dsim['NX'] = one_sim['CA']
                dsim = log(one_sim)
                if i_c == 1:
                    dsim['NX'] = one_sim['NX']
                    dsim['NX/y'] = one_sim['NX/y']
    #                 dsim['hp_CA'] = hp_filter(one_sim['CA'], lam=6.25)[1]
    #                 dsim['hp_CA_y'] = hp_filter(one_sim['CA_y'], lam=6.25)[1]
                [cycle, trend] = sm.tsa.filters.hpfilter(dsim, lamb=6.25)

                stats_stds.append(cycle[:horiz].std())
                stats_corrs.append(cycle[:horiz].corr())


            stats_stds = sum(stats_stds)/sim.shape[0]
            stats_corrs = sum(stats_corrs)/sim.shape[0]

            stats_stds['std_i_1_std_y_1'] = stats_stds['i_1']/stats_stds['y_1']
            stats_stds['std_i_2_std_y_2'] = stats_stds['i_2']/stats_stds['y_2']

            stats_stds = stats_stds[~stats_stds.isnull()]

            case_stats_stds.append(stats_stds)
            case_stats_corrs.append(stats_corrs)
        all_stds[calib_name] = case_stats_stds
        all_corrs[calib_name] = case_stats_corrs

    all_simulations.close()
    
    return [all_stds, all_corrs]


# In[16]:
simulate = False

if simulate:

    print("--------------------")
    print("Simulate normal IRFS")
    print("--------------------")
    compute_all_simulations('normal', n_exp=0, horizon=1000)
    compute_all_simulations('normal', n_exp=0, capital_scarcity=0.5, horizon=1000)
    
    print("---------------------------")
    print("Simulate normal Simulations")
    print("---------------------------")
    compute_all_simulations('normal', n_exp=1000)
    compute_all_simulations('normal', n_exp=1000, capital_scarcity=0.5)
    
    
    print("-----------------------------")
    print("Appending auxiliary variables")
    print("-----------------------------")
    
    sh = shelve.open('results/all_simulations')
    for k in sh.keys():
    #     print(k)
        sim = sh[k]
        nsim = add_variables(sim)
        sh[k] = nsim
    sh.close()
    
    


# Computing st
print("--------------------" )
print("Computing Statistics" )
print("--------------------" )
stds, corrs = get_statistics('normal')


# In[19]:


scenarios = [eval(e) for e in stds.keys()]
calib_names = [e[1] for e in scenarios]


# In[20]:

import pickle
with open('results/statistics.pickle', 'w') as f:
    pickle.dump( {'std_devs':stds, 'covariances':corrs}, f)


from numpy import *



import xlsxwriter


print("--------------------" )
print("Saving Volvatilities" )
print("--------------------" )


# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('tables/volatilities.xlsx')
bold = workbook.add_format({'bold': True})

for full_name in sorted(stds.keys()):
    calib_name = eval(full_name)[1]
    sheet = workbook.add_worksheet(name=calib_name[:30])
    sheet.write(0,0,'Autarky', bold)
    sheet.write(0,1, 'std.dev.(%)', bold)
    sheet.write(0,3,'PI', bold)
    sheet.write(0,4, 'std.dev.(%)', bold)
    sheet.write(0,6,'CI', bold)
    sheet.write(0,7, 'std.dev.(%)', bold)
    stds_A, stds_B, stds_C = stds[full_name]
    for i in range(stds_A.shape[0]):
        sheet.write(i+1,0,stds_A.index[i])
        sheet.write(i+1,1,stds_A[i]*100)
    for i in range(stds_B.shape[0]):
        sheet.write(i+1,3,stds_B.index[i])
        sheet.write(i+1,4,stds_B[i]*100)
    for i in range(stds_C.shape[0]):
        sheet.write(i+1,6,stds_C.index[i])
        sheet.write(i+1,7,stds_C[i]*100)
        
workbook.close()


# In[23]:

import xlsxwriter

print("-------------------" )
print("Saving Correlations" )
print("-------------------" )



for i,case in enumerate(['A','B','C']):
    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('tables/correlations_{}.xlsx'.format( case))
    bold = workbook.add_format({'bold': True})

    for full_name in sorted( stds.keys() ):
        calib_name = eval(full_name)[1]

        sheet = workbook.add_worksheet(name=calib_name[:30])
        corrs = all_corrs[full_name][i]
        corrs = corrs.dropna( axis=0,how='all').dropna(axis=1, how='all')

        for n in range(corrs.shape[0]):
            name = corrs.index[n]
            sheet.write(0,n+1,name,bold)
            sheet.write(n+1,0,name,bold)
            for p in range(corrs.shape[1]):
                 sheet.write(n+1,p+1,corrs.iloc[n,p])
        
    workbook.close()


# In[24]:

all_sims = stds.keys()


# # Risky Steady States

# In[25]:

from collections import OrderedDict


# In[26]:
print("------------------")
print("Saving simulations")
print("------------------")

all_simulations = shelve.open('results/all_simulations')

risky_steady_state = OrderedDict()

for full_calib in sorted( all_simulations.keys() ):
    calib_name, n_exp, scarcity = eval(full_calib)[1:4]
    if n_exp>0 or scarcity<0.99:
        continue
    stats = []
    for i,sim in enumerate( all_simulations[full_calib] ):
        case = ['A', 'B', 'C'][i]
        stats.append(sim.iloc[-1])
    risky_steady_state[calib_name] = stats

all_simulations.close()


# In[ ]:

import xlsxwriter

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('tables/risky_steady_states.xlsx')
bold = workbook.add_format({'bold': True})

for calib_name in sorted( risky_steady_state.keys()):
    print(calib_name)
    sheet = workbook.add_worksheet(name=calib_name[:30])
    sheet.write(0,0,'Autarky', bold)
    sheet.write(0,1, 'Mean', bold)
    sheet.write(0,2, 'Deviation', bold)
    sheet.write(0,4,'PI', bold)
    sheet.write(0,5, 'Mean', bold)
    sheet.write(0,6, 'Mean', bold)
    sheet.write(0,8,'CI', bold)
    sheet.write(0,9, 'Mean', bold)
    sheet.write(0,10, 'Deviation', bold)
    m_A, m_B, m_C = risky_steady_state[calib_name]
    for i in range(m_A.shape[0]):
        sheet.write(i+1,0,m_A.index[i])
        try:
            sheet.write(i+1,1,(m_A[i]))
        except:
            pass
#         sheet.write(i+1,2,s_A[i])
    for i in range(m_B.shape[0]):
        sheet.write(i+1,4,m_B.index[i])
        try:
            sheet.write(i+1,5,(m_B[i]))
        except:
            pass
#         sheet.write(i+1,6,s_B[i])
    for i in range(m_C.shape[0]):
        sheet.write(i+1,8,m_C.index[i])
        try:
            sheet.write(i+1,9,(m_C[i]))
        except:
            pass
#         sheet.write(i+1,10,s_C[i])
        
workbook.close()


# In[ ]:




# # Ergodic means

# In[ ]:

all_simulations = shelve.open('results/all_simulations')

convergence_statistics = OrderedDict()
convergence_statistics_sdev = OrderedDict()
for full_calib in sorted( all_simulations.keys() ):
    sims = all_simulations[full_calib]
    calib_name, n_exp, scarcity = eval(full_calib)[1:4]
    if n_exp==0 or scarcity<0.99:
        continue
    stats = []
    stats_sdev = []
    for case in range(3):
        sim = sims[case]
        h = sim.shape[1]-1
        tab = sim.mean(axis=0).T[h]
        tab_sdev = sim.std(axis=0).T[h]
        stats.append(tab)
        stats_sdev.append(tab_sdev)
    convergence_statistics[calib_name] = stats
    convergence_statistics_sdev[calib_name] = stats_sdev
        
all_simulations.close()


# In[ ]:

name = 'baseline_30_ez'
case = 0
mpr_1 = convergence_statistics[name][case]['r_x_1'] / convergence_statistics_sdev[name][case]['r_x_1']
mpr_2 = convergence_statistics[name][case]['r_x_2'] / convergence_statistics_sdev[name][case]['r_x_2']
(mpr_1, mpr_2)


# In[ ]:



# In[ ]:

import xlsxwriter

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('tables/convergence_statistics.xlsx')
bold = workbook.add_format({'bold': True})


for calib_name in convergence_statistics.keys():
    sheet = workbook.add_worksheet(name=calib_name[:30])
    sheet.write(0,0,'Autarky', bold)
    sheet.write(0,1, 'Mean', bold)
    sheet.write(0,2, 'Deviation', bold)
    sheet.write(0,4,'PI', bold)
    sheet.write(0,5, 'Mean', bold)
    sheet.write(0,6, 'Mean', bold)
    sheet.write(0,8,'CI', bold)
    sheet.write(0,9, 'Mean', bold)
    sheet.write(0,10, 'Deviation', bold)
    m_A, m_B, m_C = convergence_statistics[calib_name]
    s_A, s_B, s_C = convergence_statistics_sdev[calib_name]
    for i in range(m_A.shape[0]):
        sheet.write(i+1,0,m_A.index[i])
        sheet.write(i+1,1,m_A[i])
        sheet.write(i+1,2,s_A[i])
    for i in range(m_B.shape[0]):
        sheet.write(i+1,4,m_B.index[i])
        sheet.write(i+1,5,m_B[i])
        sheet.write(i+1,6,s_B[i])
    for i in range(m_C.shape[0]):
        sheet.write(i+1,8,m_C.index[i])
        sheet.write(i+1,9,m_C[i])
        sheet.write(i+1,10,s_C[i])
        
workbook.close()


# In[ ]:

# Deterministic simulations


# In[ ]:




# In[ ]:

all_simulations = shelve.open('results/all_simulations')

irfs_no_scarce = OrderedDict()
irfs_scarce = OrderedDict()

for calib_full in sorted(all_simulations.keys()):
    [scenario, calib_name, n_exp, scarcity] = eval(calib_full)
    if n_exp<=1:
        sims = all_simulations[calib_full]
        if scarcity == 1:
            irfs_no_scarce[calib_name] = sims
        else:
            irfs_scarce[calib_name] = sims
# all_simulations.close()


# In[ ]:

import xlsxwriter

# Create a workbook and add a worksheet.
for scarsity in ['', '_scarse']:
   
        for i_case,case in enumerate(['A', 'B', 'C']):

            workbook = xlsxwriter.Workbook('tables/irfs{}_{}.xlsx'.format(scarsity, case))
            bold = workbook.add_format({'bold': True})
            
            for calib_name in sorted(irfs_scarce.keys()):

                irfs = (irfs_no_scarce[calib_name] if scarsity=='' else irfs_scarce[calib_name] )
                irfs = irfs[i_case]
                
                
                sheet = workbook.add_worksheet(name=calib_name[:30])
                for q,col in enumerate(irfs.columns):
                    sheet.write(0,q+1,col, bold)
                for p,col in enumerate(irfs.index):
                    sheet.write(p+1,0,col, bold)
                for p in range(irfs.shape[0]):
                    for q in range(irfs.shape[1]):
                        try:
                            sheet.write(p+1, q+1, irfs.iloc[p,q])
                        except:
                            pass


            workbook.close()


# In[ ]:




# In[ ]:



