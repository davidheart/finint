#
# read the calibration files and outputs dependency informations for use with graphviz
#

import matplotlib
import yaml
import os
import networkx as nx

# TODO simplify graph to account for baseline calibration.

for scenario in ['calibration_long_run','calibration_normal']:

    with open('models/{}.yaml'.format(scenario)) as f:
        data = yaml.load(f)

    tasks = data['tasks']
    names = [task['name'] for task in tasks]

    links = [(task['parent'],task.get('name')) for task in tasks if task.get('parent') is not None]

    labels = []
    for task in tasks:
        label = '<u><B>{}</B></u><br/>'.format(task['name'])
        label += '<br/>'
        content = task['content']
        for k in sorted(content.keys()):
            v = content[k]
            label += '{}: {}<br/>'.format(k,v)
        label = '<{}>'.format(label)
        labels.append(label)



    H = nx.DiGraph()
    for i,n in enumerate(names): H.add_node(n, label=labels[i], shape='box')
    for l in links: H.add_edge(l[0], l[1])

    nx.write_dot(H,'results/{}.dot'.format(scenario))

    cmd = 'dot -Tsvg -o results/dependencies_{0}.svg results/{0}.dot'.format(scenario)

    os.system(cmd)
