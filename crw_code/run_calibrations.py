
def fun(start=None, **calib):

    #    fname = calib.pop('model_file')

    fname = calib.pop('model_file')
    actions = calib.pop('actions')
    from dolo import yaml_import
    model = yaml_import(fname)


    model.set_calibration(**calib)

    import crw_code.make_initial_guess

    if start is None:
        if len(model.symbols['states']) == 1:
            from crw_code.make_initial_guess import initial_dr_aut as initial_dr
        elif len(model.symbols['states']) == 2:
            if 'Complete' in model.name:
                from crw_code.make_initial_guess import initial_dr_comp as initial_dr
            else:
                from crw_code.make_initial_guess import initial_dr_aut_2 as initial_dr
        elif len(model.symbols['states']) == 3:
            from crw_code.make_initial_guess import initial_dr_int as initial_dr
        init_dr = lambda i,s:  initial_dr(model, i, s)
    else:
        if isinstance(start, dict):
            init_dr = start['solution']
        else:
            init_dr = start
        print("Using initial guess")

    save = dict(
            fname = fname,
            calib = calib,
            calibration_dict = model.calibration_dict
        )


    from crw_code.time_iteration import time_iteration
    sol = time_iteration(model, initial_guess=init_dr, verbose='full', tol=1e-6, inner_maxit=5)

    save['solution'] = sol

    if start is not None and 'values' in start:
        init_guess = start['values']
        print("Using initial guess for values")
        print(init_guess)
    else:
        init_guess = None
    from dolo.algos.dtmscc.value_iteration import evaluate_policy
    val = evaluate_policy(model, sol, verbose=True, initial_guess=init_guess)
    save['values'] = val

    return save

if __name__ == '__main__':

    import argparse

    from dolo import __version__

    def str2bool(v):
        return v.lower() in ("yes", "true", "t", "1")

    parser = argparse.ArgumentParser(description='Dolo sensitivity analysis')
    parser.add_argument('-v', '--versions', action='version', version=__version__)

    parser.add_argument('--model', type=str, help='model file')
    parser.add_argument('--refine', type=str2bool, default=True, help='refine results')
    parser.add_argument('--store', type=str, help='shelve file')
    parser.add_argument('--values', type=str2bool, default=True, help='compute values')
    parser.add_argument('--solve', type=str2bool, default=True, help='solve')

    parser.add_argument('--n_cpus', type=int, default=None, help='number of cpus to use')
    parser.add_argument('calib_file', help='calibrations file')
    args = parser.parse_args()



    import yaml
    calib_file = args.calib_file
    with open(calib_file) as f:
        calibrations = yaml.load(f)

    model_file = args.model if args.model else calibrations['options']['model']
    shelve_file = args.store if args.store else calibrations['options']['store']
    # find a consistent way to deal for that.
    if args.n_cpus is None:
        n_cpus = calibrations['options'].get('n_cpus')
        if n_cpus is None:
            n_cpus=1
    else:
        n_cpus = args.n_cpus

    print( n_cpus )
    refine = args.refine

    from dolo import yaml_import
    model = yaml_import(model_file)

    options = calibrations['options']
    tasks = calibrations['tasks']

    actions = []
    if args.solve: actions.append('solve')
    if args.values: actions.append('values')

    for t in tasks:
        t['content']['model_file'] = model_file
        t['content']['actions'] = tuple(actions)

    print("Refining : {}".format(refine))

    from tasks import parallel_run

    parallel_run(fun, tasks, max_cpus=n_cpus, store=shelve_file, refine=refine)
