import numpy

### "residuals"
def residuals(f, g, i_m, s, x, dr, P, Q, parms, indices=None):

    if indices is not None:

        i_m = i_m[indices]
        s = s[indices, :]

    NN, n_x = s.shape
    assert(i_m.shape == (NN,) )
    n_x = x.shape[1]
    assert(x.shape == (NN, n_x))

    n_ms = Q.shape[1]
    m = P[i_m, :]

    res = numpy.zeros((NN,n_x))
    parms = parms[None,:].repeat(NN, axis=0)

    for I_ms in range(n_ms):

        M = P[I_ms, :][None,:].repeat(NN, axis=0)
        prob = Q[:,I_ms][i_m]

        S = g(m, s, x, M, parms)
        XM = dr(I_ms, S)
        rr = f(m,s,x, M, S,XM, parms)
        res += prob[:, None] * rr

    return res

    ### "endresiduals"

### "empty"


def residuals_vec(f, g, i_m, s, x, dr, P, Q, parms, indices=None):

    if indices is not None:

        i_m = i_m[indices]
        s = s[indices, :]

    NN, n_x = s.shape
    assert(i_m.shape == (NN,) )
    n_x = x.shape[1]
    assert(x.shape == (NN, n_x))
    n_s = s.shape[1]
    n_ms = Q.shape[1]
    m = P[i_m, :]

    nres = numpy.zeros((n_ms*NN,n_x))
    res = numpy.zeros((NN,n_x))

#    parms = parms[None,:].repeat(NN, axis=0)

    mm = numpy.tile(m, (n_ms, 1))
    ss = numpy.tile(s, (n_ms, 1))
    xx = numpy.tile(x, (n_ms, 1))

    MM = P.repeat(NN, axis=0)
    prob = Q[i_m,:].T.ravel()

    SS = g(mm, ss, xx, MM, parms)
    XM = numpy.zeros((n_ms, NN, n_x))
    for i in range(n_ms):
        XM[i,:,:] = dr(i, SS.reshape(n_ms, NN, n_s)[i,:,:])
    XM = XM.reshape((-1, n_x))
    rr = f(mm,ss,xx, MM, SS,XM, parms)
    nres = prob[:, None] * rr
    nres = nres.reshape( (n_ms, NN, n_x) )
    for i in range(n_ms):
        res += nres[i,:,:]

    return res



### "time_iteration"
def time_iteration(model, initial_guess=None, with_complementarities=True,
                        verbose=True, orders=None, output_type='dr',
                        maxit=1000, inner_maxit=10, tol=1e-6, hook=None) :

    assert(model.model_type == 'dtmscc')

    [P, Q] = model.markov_chain

    n_ms = P.shape[0]   # number of markov states
    n_mv = P.shape[1] # number of markov variables

    x0 = model.calibration['controls']
    parms = model.calibration['parameters']
    n_x = len(x0)
    n_s = len(model.symbols['states'])

    approx = model.options['approximation_space']
    a = approx['a']
    b = approx['b']
    if orders is None:
        orders = approx['orders']

    from dolo.numeric.decision_rules_markov import MarkovDecisionRule

    mdr = MarkovDecisionRule(n_ms, a, b, orders)

    grid = mdr.grid
    N = grid.shape[0]

    controls_0 = numpy.zeros((n_ms, N, n_x))

    if initial_guess is None:
        controls_0[:,:,:] = x0[None,None,:]
    else:
        for i_m in range(n_ms):
            m = P[i_m,:][None,:]
            controls_0[i_m,:,:] = initial_guess(i_m, grid)

    ff = model.functions['arbitrage']
    gg = model.functions['transition']
    aa = model.functions['auxiliary']

    if 'controls_lb' in model.functions and with_complementarities==True:
        lb_fun = model.functions['controls_lb']
        ub_fun = model.functions['controls_ub']
        lb = numpy.zeros_like(controls_0)*numpy.nan
        ub = numpy.zeros_like(controls_0)*numpy.nan
        for i_m in range(n_ms):
            m = P[i_m,:][None,:]
            p = parms[None,:]
            m = numpy.repeat(m, N, axis=0)
            p = numpy.repeat(p, N, axis=0)

            lb[i_m,:,:] = lb_fun(m, grid, p)
            ub[i_m,:,:] = ub_fun(m, grid, p)

    else:
        with_complementarities = False


    f = lambda m,s,x,M,S,X,p: ff(m,s,x,aa(m,s,x,p),M,S,X,aa(M,S,X,p),p)
    g = lambda m,s,x,M,p: gg(m,s,x,aa(m,s,x,p),M,p)

    # mdr.set_values(controls)

    sh_c = controls_0.shape

    controls_0 = controls_0.reshape( (-1,n_x) )


    from .newton import newton, SerialDifferentiableFunction
    from .ncpsolve import ncpsolve

    err = 10
    it = 0

    if with_complementarities:
        print("Solving WITH complementarities.")
        lb = lb.reshape((-1,n_x))
        ub = ub.reshape((-1,n_x))

    if verbose:
        headline = '|{0:^4} | {1:10} | {2:8} | {3:8} | {4:3} |'.format( 'N',' Error', 'Gain','Time',  'nit' )
        stars = '-'*len(headline)
        print(stars)
        print(headline)
        print(stars)

    full_grid = numpy.tile( grid, (n_ms, 1))
    m_inds = numpy.repeat( range(n_ms), N)
    import time
    t1 = time.time()

    err_0 = numpy.nan

    verbit = (verbose == 'full')

    while err>tol and it<maxit:

        it += 1

        t_start = time.time()

        mdr.set_values(controls_0.reshape(sh_c))

        def fn(x, indices=None, diff=False):

            res = residuals_vec(f, g, m_inds, full_grid, x, mdr, P, Q, parms, indices=indices)
            if diff:
                raise Exception("Not implemented")
            else:
                return res

        dfn = SerialDifferentiableFunction(fn)

        if hook:
            hook()

        if with_complementarities:
            [controls,nit] = ncpsolve(dfn, lb, ub, controls_0, verbose=verbit, maxit=inner_maxit)
        else:
            [controls, nit] = newton(dfn, controls_0, verbose=verbit, maxit=inner_maxit)

        err = abs(controls-controls_0).max()

        err_SA = err/err_0
        err_0 = err

        controls_0 = controls

        t_finish = time.time()
        elapsed = t_finish - t_start

        if verbose:
            print('|{0:4} | {1:10.3e} | {2:8.3f} | {3:8.3f} | {4:3} |'.format( it, err, err_SA, elapsed, nit  ))

    controls_0 = controls.reshape(sh_c)

    t2 = time.time()

    if verbose:
        print(stars)
        print("Elapsed: {} seconds.".format(t2-t1))
        print(stars)


    if output_type == 'dr':
        return mdr
    elif output_type == 'controls':
        return controls_0
    else:
        raise Exception("Unsupported ouput type {}.".format(output_type))

    ### "end_time_iteration"

### "main_section"

if __name__ == '__main__':

    from dolo import *
    model = yaml_import("../../../examples/models/rbc_mfga.yaml")
    print(model.calibration['states'])
    print(model.calibration_dict)
    print(model.markov_chain)


    initial_guess_symbolic = [
        'i = delta*k',
        'n = 0.33'
    ]

    from dolo.compiler.function_compiler_ast import compile_function_ast
    from dolo.compiler.function_compiler import standard_function

    arg_names = [
        ['markov_states',0,'m'],
        ['states',0,'s'],
        ['parameters',0,'p']
    ]

    fun = compile_function_ast(initial_guess_symbolic, model.symbols, arg_names,'initial_guess')
    ff = standard_function(fun, len(initial_guess_symbolic))

    sol = time_iteration(model, initial_guess=ff)
