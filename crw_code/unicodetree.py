#!/usr/bin/env python
# -*- coding: utf-8 -*-



def draw_tree(node, child_iter=lambda n: n.children, text_str=lambda u: unicode(u)):
    return _draw_tree(node, u'', child_iter, text_str)


class UniString:
    def __init__(self):
        self.string = u''

    def write(self,h):
        self.string = self.string + h

    def getvalue(self):
        return self.string

def _draw_tree(node, prefix, child_iter, text_str):

    buf = u''

    children = list(child_iter(node))

    # check if root node
    if prefix:
        buf += prefix[:-3]
        buf += u'  +--'
    buf += text_str(node)
    buf += u'\n'

    for index, child in enumerate(children):
        if index+1 == len(children):
            sub_prefix = prefix + u'   '
        else:
            sub_prefix = prefix + u'  |'

        buf += _draw_tree(child, sub_prefix, child_iter, text_str)

    return buf


if __name__ == '__main__':
    class Node(object):
        def __init__(self, name, children):
            self.name = name
            self.children = children

        def __str__(self):
            return self.name

    root = Node(u'root', [
        Node(u'sub1', []),
        Node(u'sub2', [
            Node(u'sub2sub1\2713', [])
        ]),
        Node(u'sub3', [
            Node(u'sub3sub1', [
                Node(u'sub3sub1sub1', [])
            ]),
            Node(u'sub3sub2', [])
        ])
    ])

    print (draw_tree(root))
