import os

for case in ['A','B','C']:
    for scenario in ["normal", "long_run"]:

        cmd = "python crw_code/run_calibrations.py models/calibration_{scenario}.yaml --model=models/integration_{case}.yaml --refine=true --store=solutions_{case}.hdf5 --n_cpus=4"
        cmd = cmd.format(case=case, scenario=scenario)
        print(cmd)
        os.system(cmd)
#
# python crw_code/run_calibrations.py models/calibration_normal.yaml --model=models/integration_A.yaml --refine=true --store=solutions_A.hdf5 --n_cpus=4
# python crw_code/run_calibrations.py models/calibration_normal.yaml --model=models/integration_B.yaml --refine=true --store=solutions_B.hdf5 --n_cpus=4
# python crw_code/run_calibrations.py models/calibration_normal.yaml --model=models/integration_C.yaml --refine=true --store=solutions_C.hdf5 --n_cpus=4
# python crw_code/run_calibrations.py models/calibration_long_run.yaml --model=models/integration_A.yaml --refine=true --store=solutions_A.hdf5 --n_cpus=4
# python crw_code/run_calibrations.py models/calibration_long_run.yaml --model=models/integration_B.yaml --refine=true --store=solutions_B.hdf5 --n_cpus=4
# python crw_code/run_calibrations.py models/calibration_long_run.yaml --model=models/integration_C.yaml --refine=true --store=solutions_C.hdf5 --n_cpus=4
