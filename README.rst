Companion code for *Financial integration in a risky world* by Nicolas Coeurdacier, Helene Rey and Pablo Winant
===============================================================================================================

Before trying to replicate the results, it is advised to read the the documentation which can be browsed `online <https://bitbucket.org/albop/finint/src/38d34d35b4e72e2b5e18fe9342040bb19f54077f/output/full_doc.rst?at=master&fileviewer=file-view-default>`_ or downloaded as a `pdf <https://bitbucket.org/albop/finint/raw/0be0f99906dff48fb706ea3b8ba6e5a82d30a543/output/full_doc.pdf>`_.

Requirements
------------

python packages (anaconda >=2)

dolo = ../

numba >= 0.13

Quickly replicating the results:
--------------------------------

Also located in the root directories, the following scripts replicate
all results at once:

-  ``run_all_solutions.sh``
-  ``run_all_simulations.sh``

Since these two commands take a very long time to produce the results,
it is almost certainly preferable to download them instead:

-  ``python download_all_solutions.py``
-  ``python download_all_simulations.py``

If the solutions are downloaded, before solution routines are run, the
downloaded data will be used as initial guesses, so that the program
will actually verify that they satisfy the optimality conditions.

After solutions and the simulations have been computed/downloaded, in
order to produce the tables and graphs, run respectively:

-  ``make_all_graphs.sh``
-  ``make_all_tables.sh``
