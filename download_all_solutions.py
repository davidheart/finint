import os

files = [
    'solutions_A.hdf5',
    'solutions_B.hdf5',
    'solutions_C.hdf5'
]

for f in files:
    cmd = 'wget "https://dl.dropboxusercontent.com/u/21505234/crw_data/{}?dl=1" -O "results/{}"'
    os.system(cmd.format(f,f))
