import os

files = [
    'correlations.hdf5',
    'risky_steady_states.hdf5',
    'simulations.hdf5',
    'std_deviations.hdf5',
    'welfares.hdf5',
]

for f in files:
    cmd = 'wget "https://dl.dropboxusercontent.com/u/21505234/crw_data/{}?dl=1" -O "results/{}"'
    os.system(cmd.format(f,f))
